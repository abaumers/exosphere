# Architecture Decision Records

See: <https://adr.github.io/>

1. [ADR 1: Choose technologies for automated, end-to-end browser tests](choose_technologies_for_browser_tests.md)
2. [ADR 2: Configuration management for instance provisioning](config-mgt-instance-provisioning.md)