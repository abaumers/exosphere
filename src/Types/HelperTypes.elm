module Types.HelperTypes exposing (Hostname, Password, Url, Uuid)


type alias Url =
    String


type alias Hostname =
    String


type alias Uuid =
    String


type alias Password =
    String
